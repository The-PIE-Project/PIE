import sys
import os

pie_file = sys.argv[1]
dir_path = os.path.dirname(os.path.realpath(__file__)) + "/"

with open("out.py", 'w') as target:
    target.write("# ----------------------------------------------\n# This file was automatically generated with PIE\n# ----------------------------------------------\n")

def doVarType(line, type):
    if line.rstrip().startswith(type + " "):
        line = line.rstrip()[len(type)+1:] + "\nif not isinstance(" + line.rstrip().split(" ")[1] + ", " + type + "):\n\tprint(\"Error: You defined " + line.rstrip().split(" ")[1] + " as a " + type + " even through it is another type.\")\n\texit()"
    return line

def interpret(f):
    define_dict = {}

    quoted = False
    infunction = False

    for line in f:
        with open("out.py", 'a') as target:
            tok = ""
            if line.rstrip().startswith("#"):
                if line.strip() == "#include <pieLibs_core>":
                    with open(dir_path + "coreLibs.py", "r") as fi:
                        interpret(fi)
                if line.strip().split(" ")[0] == "#define":
                    define_dict[line.strip().split(" ")[1]] = line.strip().split(" ")[2]

                line = ""
            else:
                if line.rstrip() == "}":
                    line = ""
                    infunction = False
                else:
                    if not line.rstrip().startswith("\t"):
                        if infunction:
                            line = "\t" + line

                    if line.rstrip().startswith("var "):
                        line = line.rstrip()[4:]

                    if line.rstrip().startswith("// "):
                        line = ""

                    line = doVarType(line, "str")
                    line = doVarType(line, "int")
                    line = doVarType(line, "bool")

                    if line.strip().endswith(";"):
                        line = line.rstrip()[:-1]

                    for c in line.rstrip():
                        if c == "\"" or c == "'":
                            quoted = not quoted
                        else:
                            if not quoted:
                                tok += c
                                if "} else" in tok:
                                    line = line.replace("} else", "}else")
                                    tok = ""
                                if "function" in tok:
                                    line = line.replace("function", "def")
                                    tok = ""
                                if "void" in tok:
                                    line = line.replace("void", "def")
                                    tok = ""
                                if "{" in tok:
                                    line = line.replace("{", ":")
                                    tok = ""
                                    infunction = True
                                if "}" in tok:
                                    line = line.replace("}", "")
                                    tok = ""
                                    infunction = False
                                if "include" in tok:
                                    line = line.replace("include", "import")
                                    tok = ""
                                if "false" in tok:
                                    line = line.replace("false", "False")
                                    tok = ""
                                for z in define_dict:
                                    if define_dict[z] in tok:
                                        line = line.replace(define_dict[z], z)
                                        tok = ""
            target.write(line + "\n")

with open(pie_file) as f:
    interpret(f)

with open('out.py', 'r+') as f:
    content = f.read()
    f.seek(0)
    f.truncate()
    f.write((content.replace('\n\n', '\n')).replace('\n\n', '\n'))

os.system("python out.py")