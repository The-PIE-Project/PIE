include validators
include requests

function find_in(str, var) {
    if str in var {
        return True
    }else {
        return False
    }
}

function replace_in_file(filename, oldvalue, newvalue) {
    with open(filename, 'r+') as f {
        content = f.read()
        f.seek(0)
        f.truncate()
        f.write(content.replace(oldvalue, newvalue))
    }
}

function file_get_contents(f) {
	if validators.url(f) {
		return requests.get(f).text;
	}else {
		with open(f, "r+") as file1 {
			return file1.read();
		}
	}
}